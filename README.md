
The App Icon is Licensed under Apache by material.io
All Files in the lib directory are licensed under the given terms of the original creator.
* jQuery (jquery.com): MIT
* bootstrap (getbootstrap.com): MIT
* js-cookie (https://github.com/js-cookie/js-cookie): MIT
* sweetalert (https://sweetalert.js.org/): MIT

Thanks to all the people that made these wonderfull libs, without them, this project wouldnt work!
