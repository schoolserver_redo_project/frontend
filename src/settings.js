hereyougetyourcookies();
function hereyougetyourcookies() {
  if (Cookies.get("cookies_accepted") != "true") {
    console.log("Cookies not accepted.");
    swal({
      title: "Cookies are yummy",
      text:
        "We use Cookies. Like everyone. You wanna enter all your settings every time again? No? Thats why we need Cookies.",
      icon: "warning",
      buttons: true,
      //dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        Cookies.set("cookies_accepted", true, { expires: 1200 });
        main();
      } else {
        swal("I am sorry, but this website depends on cookies.", {
          buttons: false,
          timer: 3000,
        }).then((test) => {
          hereyougetyourcookies();
        });
      }
    });
  } else {
    main();
  }
}

// #BiggestIneffientCodeInWorld (theming)
function changestyle(scheme) {
  document.getElementById("pagestyle").setAttribute("href", scheme);
}

window.submit = function () {
  var server = document.getElementById("server").value;
  var schoolclass = document.getElementById("schoolclass").value;
  var dark = document.getElementById("themetoggler").checked;
  if (server.slice(-1) != "/") {
    server = server + "/";
  }
  Cookies.set("server", server, { expires: 1200 });
  Cookies.set("schoolclass", schoolclass, { expires: 1200 });
  Cookies.set("dark", dark, { expires: 1200 });
  window.open("./", "_self");
};

function showinfo() {
  var textdiv = document.createElement("div");
  textdiv.innerHTML = `
  This App is part of the <a href="https://gitlab.com/schoolserver_redo_project">schoolserver_redo_project</a>.<br>
  The source code for this webapp can be found <a href="https://gitlab.com/schoolserver_redo_project/frontend/">here</a>.
  It is developed by <a href="https://alwin.kat-zentrale.de/">Niwla23.<a/><br>
  If you have HTML, CSS, JS or python experience, you can contribute at the link above!<br>

  If you find an issue or have a Feature Request, 
  feel free to contact me at telegram: <a href=https://t.me/niwla23>@niwla23</a>
   or file an Issue <a href="https://gitlab.com/groups/schoolserver_redo_project/-/issues">here</a>.<br>
  <br>
  Technical Inforamtion and Tutorials how to setup your own scrap server can be found <a href="docs.vertreter.kat-zentrale.de/">here</a>
  <br><a href=http://legal.kat-zentrale.de/>Click here for our german Privacy Policy.</a>
  `;
  swal({
    title: "About this App",
    content: textdiv,
  });
}

function main() {
  if (Cookies.get("dark") === "true") {
    changestyle("style-dark.css");

    $("#spinner").removeClass("text-dark");
    $("#spinner").addClass("text-light");
  } else {
    changestyle("style-light.css");
    $("#spinner").removeClass("text-light");
    $("#spinner").addClass("text-dark");
  }

  // actual settings functions
  document.getElementById("server").value = Cookies.get("server");
  document.getElementById("schoolclass").value = Cookies.get("schoolclass");
  // document.getElementById("themetoggler").checked = Cookies.get("dark")
  if (Cookies.get("dark") === "true") {
    $("#themetoggler").bootstrapToggle("on", true);
  } else {
    $("#themetoggler").bootstrapToggle("off", true);
  }
}
