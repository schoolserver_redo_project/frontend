let searchParams = new URLSearchParams(window.location.search);

hereyougetyourcookies()
function hereyougetyourcookies() {
  if (Cookies.get("cookies_accepted") != "true") {
    console.log("Cookies not accepted.");
    swal({
      title: "Cookies are yummy",
      text:
        "We use Cookies. Like everyone. You wanna enter all your settings every time again? No? Thats why we need Cookies.",
      icon: "warning",
      buttons: true,
      //dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        Cookies.set("cookies_accepted", true);
        main();
      } else {
        swal("I am sorry, but this website depends on cookies.", {
          buttons: false,
          timer: 3000,
        }).then((test) => {
          hereyougetyourcookies()
        })
      }
    });
  } else {
    main();
  }
}

function changestyle(scheme) {
  document.getElementById("pagestyle").setAttribute("href", scheme);
}

function addtosublist(time, content) {
  $("#sublist").append(
    '<li><div class="card bg-primary text-white margin-top-default rounded"><div class="card-header">' +
      time +
      '</div><div class="card-body">' +
      content +
      "</div></div></li>"
  );
}

function cant_connect_message() {
  cant_connect_text = `
    Es konnte keine Verbindung zum Server hergestellt werden.
    Wenn du dir sicher bist, dass die URL richtig ist und du eine funktionierende Internetverbindung hast,
    kontaktiere deinen Server Admin. Prüfe auch das die Server-URL mit https:// anfängt und der Server https unterstützt!
    `;
  if (config_standalone === true) {
    var close_text = "Settings";
  } else {
    var close_text = "Close";
  }

  swal({
    title: "Error",
    text: cant_connect_text,
    icon: "error",
    button: close_text,
  }).then((value) => {
    if (config_standalone === true) {
      window.open("settings.html", "_self");
    }
  });
}

function update_next_day() {
  $("#daytoggler").bootstrapToggle("on", true);
  document.getElementById("sublist").innerHTML = "";
  var jqXHR = $.get(config_server + "plan/tomorrow/" + config_class, function (
    data,
    status
  ) {
    for (index = 0; index < data.plan.length; index++) {
      addtosublist(data.plan[index].time, data.plan[index].content);
    }
    document.getElementById("date").innerHTML = data.date;
    document.getElementById("header").innerHTML = data.schoolclass;
    document.getElementById("daychanger2").hidden = false;
    if ($("#sublist li").length === 0) {
      $("#sublist").append(
        "<br>Es steht nichts auf dem Vertretungsplan für diese Klasse."
      );
    }
  });
  jqXHR.fail(function () {
    //$("#errormodal").modal()
    cant_connect_message();
  });
}

function update_today() {
  $("#daytoggler").bootstrapToggle("off", true);
  document.getElementById("sublist").innerHTML = "";
  var jqXHR = $.get(config_server + "plan/today/" + config_class, function (
    data,
    status
  ) {
    for (index = 0; index < data.plan.length; index++) {
      addtosublist(data.plan[index].time, data.plan[index].content);
    }
    document.getElementById("date").innerHTML = data.date;
    document.getElementById("header").innerHTML = data.schoolclass;
    document.getElementById("daychanger2").hidden = false;
    if ($("#sublist li").length === 0) {
      $("#sublist").append(
        "<br>Es steht nichts auf dem Vertretungsplan für diese Klasse"
      );
    }
  });
  jqXHR.fail(function () {
    cant_connect_message();
  });
}

function main() {
  BgRed = "\x1b[41m";
  FgYellow = "\x1b[33m";

  console.log(
    FgYellow,
    "Welcome you little Hacker! Want to help develop this project?"
  );
  console.log("https://gitlab.com/schoolserver_redo_project");

  config_server = searchParams.get("server");
  config_class = searchParams.get("class");
  config_standalone = searchParams.has("standalone");

  if (searchParams.get("sclass") != "" && searchParams.get("sclass") != null) {
    Cookies.set("schoolclass", searchParams.get("sclass"), { expires: 1200 });
  }

  if (searchParams.get("sserver") != "" && searchParams.get("sserver") != null) {
    Cookies.set("server", searchParams.get("sserver"), { expires: 1200 });
  }

  if (config_server === null) {
    config_standalone = true;
  }

  window.onload = () => {
    "use strict";

    if ("serviceWorker" in navigator) {
      navigator.serviceWorker.register("./sw.js");
    }
  };

  if (searchParams.has("light")) {
    changestyle("style-light.css");
    $("#spinner").removeClass("text-light");
    $("#spinner").addClass("text-dark");
  }

  if (Cookies.get("dark") === "true") {
    changestyle("style-dark.css");

    $("#spinner").removeClass("text-dark");
    $("#spinner").addClass("text-light");
  } else {
    changestyle("style-light.css");
    $("#spinner").removeClass("text-light");
    $("#spinner").addClass("text-dark");
  }

  if (config_standalone === true) {
    config_server = Cookies.get("server");
    config_class = Cookies.get("schoolclass");
    if (
      Cookies.get("server") === "" ||
      Cookies.get("server") === undefined ||
      Cookies.get("server") === "undefined/"
    ) {
      swal({
        title: "Configuration Required",
        text: "You need to do some settings before using this!",
        icon: "success",
        button: "Ok, take me there!",
      }).then((value) => {
        window.open("settings.html", "_self");
      });
    }
    $("#settingsBtn").css("display", "unset");
  }

  update_next_day();

  window.handleSwitch = function (element) {
    if (element.checked === true) {
      update_next_day();
    } else {
      update_today();
    }
  };
}
