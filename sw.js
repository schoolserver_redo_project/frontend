var cacheName = 'vertreter-cache-v1';
var filesToCache = [
  '/',
  'index.html',
  'settings.html',
  'style.css',
  'style-light.css',
  'style-dark.css',
  'src/script.js',
  'src/settings.js',
  'lib/js/bootstrap4-toggle.js',
  'lib/js/jquery.min.js',
  'lib/js/js.cookie.min.js',
  'lib/js/sweetalert.min.js',
  'lib/css/bootstrap.min.css',
  'lib/css/bootstrap.min.css.map',
  'lib/css/bootstrap4-toggle.min.css',
];

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

/* Serve cached content when offline */
self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});